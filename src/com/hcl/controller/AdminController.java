package com.hcl.controller;

import java.util.List;

import com.hcl.dao.IAdminDao;
import com.hcl.dao.IAdminImpl;
import com.hcl.model.Admin;
import com.hcl.model.TouristInfo;

public class AdminController {
	int result;
	IAdminDao dao = new IAdminImpl();

	public int adminAuthentication(String uid, String password) {
		Admin admin = new Admin(uid, password); // setter- value of object
		return dao.adminAuthentication(admin);
	}

	public List<TouristInfo> viewAllTourists() {
		return dao.viewAllTourists();

	}

	public int addTourist(int tid, String tname, long mobile) {
		TouristInfo info = new TouristInfo(tid, tname, mobile);
		return dao.addTourist(info);

	}

	public int editMob(int tid, long mobile) {
		TouristInfo info = new TouristInfo();
		info.setTid(tid);
		info.setMobile(mobile);
		return dao.editMob(info);
	}

	public int editName(int tid, String tname) {
		TouristInfo info = new TouristInfo();
		info.setTid(tid);
		info.setTname(tname);
		return dao.editName(info);

	}

	public int editNameMob(int tid, String tname, long mobile) {
		TouristInfo info = new TouristInfo(tid, tname, mobile);
		return dao.editNameMob(info);
	}

	public int removeTourist(int tid) {
		TouristInfo info = new TouristInfo();
		info.setTid(tid);
		return dao.removeTourist(info);
	}

}
