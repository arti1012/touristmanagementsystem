package com.hcl.dao;

import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.TouristInfo;

public interface IAdminDao {
	public int adminAuthentication(Admin admin);

	public List<TouristInfo> viewAllTourists();

	public int addTourist(TouristInfo info);

	public int editMob(TouristInfo info);

	public int editName(TouristInfo info);

	public int editNameMob(TouristInfo info);

	public int removeTourist(TouristInfo info);

}
