package com.hcl.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.hcl.model.Admin;
import com.hcl.model.TouristInfo;
import com.hcl.util.Db;
import com.hcl.util.Query;

public class IAdminImpl implements IAdminDao {
	PreparedStatement pst = null;
	ResultSet rs = null;
	int result;

	@Override
	public int adminAuthentication(Admin admin) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.adminAuth);
			pst.setString(1, admin.getUid());
			pst.setString(2, admin.getPassword());
			pst.executeQuery();
			rs = pst.executeQuery();
			while (rs.next()) {
				result++;
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in Admin Authentication");
		} finally {
			try {
				pst.close();
				rs.close();
				Db.getConnection().close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return result;
	}

	@Override
	public List<TouristInfo> viewAllTourists() {
		List<TouristInfo> list = new ArrayList<TouristInfo>(); // list of objects
		try {
			pst = Db.getConnection().prepareStatement(Query.viewAll);
			rs = pst.executeQuery();
			while (rs.next()) {
				TouristInfo info = new TouristInfo(rs.getInt(1), rs.getString(2), rs.getLong(3));// setter
				list.add(info);
			}
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in view All Tourists");
		} finally {
			try {
				Db.getConnection().close();
				rs.close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return list;
	}

	@Override
	public int addTourist(TouristInfo info) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.addTourist);
			pst.setInt(1, info.getTid());
			pst.setString(2, info.getTname());
			pst.setLong(3, info.getMobile());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exception occurs in Add Tourist");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}

		return result;
	}

	@Override
	public int editMob(TouristInfo info) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editMob);
			pst.setLong(1, info.getMobile());
			pst.setInt(2, info.getTid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exceotion occurs in edit Mobile Number");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return result;
	}

	@Override
	public int editName(TouristInfo info) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editName);
			pst.setString(1, info.getTname());
			pst.setInt(2, info.getTid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exceotion occurs in edit Name");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return result;
	}

	@Override
	public int editNameMob(TouristInfo info) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.editNameMob);
			pst.setString(1, info.getTname());
			pst.setLong(2, info.getMobile());
			pst.setInt(3, info.getTid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exceotion occurs in edit Name and Mobile Number");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return result;
	}

	@Override
	public int removeTourist(TouristInfo info) {
		result = 0;
		try {
			pst = Db.getConnection().prepareStatement(Query.removeTourist);
			pst.setInt(1, info.getTid());
			result = pst.executeUpdate();
		} catch (ClassNotFoundException | SQLException e) {
			System.out.println("Exceotion occurs in edit Name and Mobile Number");
		} finally {
			try {
				Db.getConnection().close();
				pst.close();
			} catch (ClassNotFoundException | SQLException e) {
			}
		}
		return result;

	}
}
