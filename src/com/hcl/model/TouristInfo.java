package com.hcl.model;

public class TouristInfo {
	private Integer tid;
	private String tname;
	private Long mobile;

	public TouristInfo() {
		// TODO Auto-generated constructor stub
	}

	public TouristInfo(Integer tid, String tname, Long mobile) {
		super();
		this.tid = tid;
		this.tname = tname;
		this.mobile = mobile;
	}

	public Integer getTid() {
		return tid;
	}

	public void setTid(Integer tid) {
		this.tid = tid;
	}

	public String getTname() {
		return tname;
	}

	public void setTname(String tname) {
		this.tname = tname;
	}

	public Long getMobile() {
		return mobile;
	}

	public void setMobile(Long mobile) {
		this.mobile = mobile;
	}

}
