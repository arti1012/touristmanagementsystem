package com.hcl.util;

public class Query {
	public static String adminAuth = "select * from admin where uid=? and password=?";
	public static String viewAll = "select * from tourist_info";
	public static String addTourist = "insert into tourist_info values(?,?,?)";
	public static String editMob = "update tourist_info set mobile=? where tid=?";
	public static String editName = "update tourist_info set tname=? where tid=?";
	public static String editNameMob = "update tourist_info set tname=?,mobile=? where tid=?";
	public static String removeTourist = "delete from tourist_info where tid=?";

}
