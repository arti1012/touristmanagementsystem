package com.hcl.view;

import java.util.List;
import java.util.Scanner;

import com.hcl.controller.AdminController;
import com.hcl.model.TouristInfo;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter User id");
		String uid = sc.nextLine();
		System.out.println("Enter Password");
		String password = sc.nextLine();
		AdminController controller = new AdminController();
		int result = 0;
		result = controller.adminAuthentication(uid, password);
		if (result > 0) {
			System.out.println("Hello " + uid + ", Welcome to Admin Page");
			int cont = 0;
			do {
				System.out.println("1.Add Tourist 2.Remove Tourist 3.Update Tourist 4.View All Tourists ");
				int option = sc.nextInt();
				int tid = 0;
				String tname = "";
				long mobile = 0;
				switch (option) {
				case 1:
					System.out.println("Enter Tourist ID");
					tid = sc.nextInt();
					sc.nextLine();
					System.out.println("Enter Tourist Name");
					tname = sc.nextLine();
					System.out.println("Enter Tourist Mobile Number");
					mobile = sc.nextLong();
					result = controller.addTourist(tid, tname, mobile);
					if (result > 0) {
						System.out.println("Tourist bearing Id " + tid + " Added Successfully!!!");
					}
					break;
				case 2:
					System.out.println("Enter Tourist Id to remove record from database");
					tid = sc.nextInt();
					result = controller.removeTourist(tid);
					System.out.println((result > 0) ? +tid + " Removed Successfully" : "removing "+tid + " failed");
					break;
				case 3:
					System.out.println("1.Update Mobile Number 2.Update Name 3. Update Name and Mobile Number");
					option = sc.nextInt();
					if (option == 1) {
						System.out.println("Enter Tourist Id");
						tid = sc.nextInt();
						System.out.println("Enter Tourist Mobile Number");
						mobile = sc.nextLong();
						result = controller.editMob(tid, mobile);
						System.out.println((result > 0) ? "Mobile Number Updated Successfully"
								: "Mobile Number not Updated Successfully");
					} else if (option == 2) {
						System.out.println("Enter Tourist Id");
						tid = sc.nextInt();
						sc.nextLine();
						System.out.println("Enter Tourist Name");
						tname = sc.next();
						result = controller.editName(tid, tname);
						System.out
								.println((result > 0) ? "Name Updated Successfully" : "Name not Updated Successfully");

					} else if (option == 3) {
						System.out.println("Enter Tourist Id");
						tid = sc.nextInt();
						sc.nextLine();
						System.out.println("Enter Tourist Name");
						tname = sc.nextLine();
						System.out.println("Enter Tourist Mobile Number");
						mobile = sc.nextLong();
						result = controller.editNameMob(tid, tname, mobile);
						System.out.println((result > 0) ? "Name and Mobile Number Updated Successfully"
								: "Name and Mobile Number not Updated Successfully");
					} else {
						System.out.println("Invalid Update Choice Selected");
					}
					break;
				case 4:
					List<TouristInfo> list = controller.viewAllTourists();
					if (list.size() > 0) {
						System.out.println("tid,tname,mobile");
						for (TouristInfo t : list) {
							System.out.println(t.getTid() + "," + t.getTname() + "," + t.getMobile());// getter
						}
					} else {
						System.out.println("Records not found");
					}
					break;
				default:
					System.out.println("Invalid choice");
				}
				System.out.println("press 1 to continue");
				cont = sc.nextInt();
			} while (cont == 1);
		} else {
			System.out.println("Incorrect User Id or Password");
		}
		System.out.println("Thanku for using this Application");
		sc.close();

	}

}
